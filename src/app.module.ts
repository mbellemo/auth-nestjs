import { DynamicModule, Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { mongooseConfig } from './config/mongoose.config';

@Module({
  imports: [
    AuthModule,
    MongooseModule.forRoot('mongodb://localhost/auth', mongooseConfig),
    UsersModule,
  ],
})
export class AppModule {}
