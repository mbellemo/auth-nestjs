import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { UsersRepository } from './users.repository';
import { SearchUserDto } from './dto/search-user.dto';
import { FindUserDto } from './dto/find-user.dto';
import { NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';

const mockUser = {
  username: 'email1@test.com',
  password: 'fd32d3e'
};

const mockUserList = [
  {
    username: 'email1@test.com',
    isActive: null,
  },
  {
    username: 'email2@test.com',
    isActive: true,
  },
  {
    username: 'email3@test.com',
    isActive: false,
  }
];

const mockUsersRepository = () => ({
  createUser: jest.fn(),
  deleteUserById: jest.fn(),
  getUsers: jest.fn(),
  getUserById: jest.fn(),
});

describe('UsersService', () => {
  let usersService: UsersService;
  let usersRepository: UsersRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: UsersRepository, useFactory: mockUsersRepository }
      ],
    }).compile();

    usersService = await module.get<UsersService>(UsersService);
    usersRepository = await module.get<UsersRepository>(UsersRepository);
  });

  it('should be defined', () => {
    expect(usersService).toBeDefined();
  });

  describe('createUser', () => {
    it('creates a user using the repository', async () => {
      expect(usersRepository.createUser).not.toHaveBeenCalled();
      const createUserDto: CreateUserDto = mockUser;
      const spy = jest.spyOn(usersRepository, 'createUser')
        .mockImplementation(() => Promise.resolve(mockUser as any));
      const user = await usersService.createUser(createUserDto);
      expect(usersRepository.createUser).toHaveBeenCalledWith(createUserDto);
      expect(user).toEqual(mockUser);
    });
  });

  describe('deleteUserById', () => {
    it('successfully deletes a user from the repository', async () => {
      expect(usersRepository.deleteUserById).not.toHaveBeenCalled();
      const findUserDto: FindUserDto = { id: '0' };
      const spy = jest.spyOn(usersRepository, 'deleteUserById')
        .mockImplementation(() => Promise.resolve(mockUserList[0] as any));
      const user = await usersService.deleteUserById(findUserDto);
      expect(usersRepository.deleteUserById).toHaveBeenCalledWith(findUserDto);
      expect(user).toEqual(mockUserList[0]);
    });

    it('throws an error as user is not found', async () => {
      expect(usersRepository.deleteUserById).not.toHaveBeenCalled();
      const findUserDto: FindUserDto = { id: '0' };
      const spy = jest.spyOn(usersRepository, 'deleteUserById')
        .mockResolvedValue(null);
      expect(usersService.deleteUserById(findUserDto)).rejects.toThrow(NotFoundException);
      expect(usersRepository.deleteUserById).toHaveBeenCalled();
    });

  });

  describe('getUsers', () => {
    it('gets all users from the repository', async () => {
      expect(usersRepository.getUsers).not.toHaveBeenCalled();
      const searchUserDto: SearchUserDto = { username: null, isActive: null };
      const spy = jest.spyOn(usersRepository, 'getUsers')
        .mockImplementation(() => Promise.resolve(mockUserList as any));
      const users = await usersService.getUsers(searchUserDto);
      expect(usersRepository.getUsers).toHaveBeenCalledWith(searchUserDto);
      expect(users).toEqual(mockUserList);
    });
  });

  describe('getUserById', () => {
    it('successfully retrieves a user from the repository', async () => {
      expect(usersRepository.getUserById).not.toHaveBeenCalled();
      const findUserDto: FindUserDto = { id: '0' };
      const spy = jest.spyOn(usersRepository, 'getUserById')
        .mockImplementation(() => Promise.resolve(mockUserList[0] as any));
      const user = await usersService.getUserById(findUserDto);
      expect(usersRepository.getUserById).toHaveBeenCalledWith(findUserDto);
      expect(user).toEqual(mockUserList[0]);
    });

    it('throws an error as user is not found', async () => {
      expect(usersRepository.getUserById).not.toHaveBeenCalled();
      const findUserDto: FindUserDto = { id: '0' };
      const spy = jest.spyOn(usersRepository, 'getUserById')
        .mockResolvedValue(null);
      expect(usersService.getUserById(findUserDto)).rejects.toThrow(NotFoundException);
      expect(usersRepository.getUserById).toHaveBeenCalled();
    });

  });
});
