import { IsBoolean, IsEmail, IsOptional, IsBooleanString } from 'class-validator';

export class SearchUserDto {
  @IsEmail()
  @IsOptional()
  username: String;

  @IsBooleanString()
  @IsOptional()
  isActive: Boolean;
}
