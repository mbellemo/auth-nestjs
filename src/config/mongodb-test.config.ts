import { DynamicModule } from '@nestjs/common'
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

const mongod = new MongoMemoryServer();

const mongooseConfig: MongooseModuleOptions = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

export default (customOpts: any = {}) => MongooseModule.forRootAsync({
    useFactory: async () => {
        const port = await mongod.getPort();
        const database = await mongod.getDbName();

        return {
            type: 'mongodb',
            host: '127.0.0.1',
            port,
            database,
            entities: [__dirname + '/**/*.entity{.ts,.js}'],
            ...mongooseConfig,
        };
    },
});